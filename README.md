# GeoJsonBikeFluidityIndex

[![PyPI license](https://img.shields.io/pypi/l/ansicolortags.svg)](https://pypi.python.org/pypi/ansicolortags/)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)


A "**fluidity index**" calculator of **GPX** records, which generate ready-to-use [GeoJSON](https://geojson.org/) (for example, in [uMap](https://umap.openstreetmap.fr/fr/)).
Project initialized during the 2020's Hackathon "vélo et numérique" @ Rennes[FR].
* Bike, digital, commute, trip segment, responsible behaviors, greenline...

![Demo image](img/demo.png)

## How to

**What do I need** ?

* **A valid `.gpx` file, or directory of `.gpx` including trackpoints `trkpt` with at least:**

**latitude** and **longitude** values

`<trkpt lat="48.1564850" lon="-1.5771090">`
 

**timestamp** of each `trkpt` 

`<time>2019-07-25T15:17:00Z</time>` 

* **Python installed on your system.** 

## Run the project

Run `note_fluid.py` in CLI, with a single `.gpx` file or a whole directory.

> Example: 
`python note_fluide.py -out {output_file_path/name}.json  {input_path_file/name}.gpx`

The output file(s) will be in GeoJSON format `.json`, which you can use further in map visualisation tools (JOSM, uMap...
)

## Setup output in `note_fluid.py`

* **To-be-done** : change the **segment's pass**, in metrical unit **(meters)**. 

> Example: if the segment's pass is set to `200`, each segment'll estimate his own fluidity index over 200 meters.

## Display output file `.json` on a map
Even if multiple files are included in the programm, only one single will be created including all the gpx paths, as one & only path with segments.

You can import the output `.json` file on map. Dev tests are made with [uMap](https://umap.openstreetmap.fr/fr/) (see picture above).
