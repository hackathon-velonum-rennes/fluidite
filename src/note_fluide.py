import math

import gpx_parser
import pandas as pd


def haversine(coord1, coord2):
    R = 6372800  # Earth radius in meters
    lat1, lon1 = coord1
    lat2, lon2 = coord2

    phi1, phi2 = math.radians(lat1), math.radians(lat2)
    dphi = math.radians(lat2 - lat1)
    dlambda = math.radians(lon2 - lon1)

    a = (
        math.sin(dphi / 2) ** 2
        + math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2
    )

    return 2 * R * math.atan2(math.sqrt(a), math.sqrt(1 - a))


def read_gpx_dataframe(file_name):

    with open(file_name, "r") as gpx_file:
        gpx = gpx_parser.parse(gpx_file)

    speed_points = []

    for track in gpx:
        for segment in track:
            previous_point = None
            t = 0
            total_distance = 0
            for point in segment:
                if previous_point is not None:
                    distance = haversine(
                        (previous_point.latitude, previous_point.longitude),
                        (point.latitude, point.longitude),
                    )
                    delta_time = previous_point.time_difference(point)
                    if delta_time == 0:
                        continue
                    t += delta_time
                    total_distance += distance

                    speed = distance / delta_time * 3600 / 1000
                    speed_points.append((t, distance, total_distance, speed))
                previous_point = point

    df = pd.DataFrame(speed_points)
    df.columns = ["time", "seg_dist", "dist", "speed"]

    return df


from collections import namedtuple

GpxPoint = namedtuple(
    "GpxPoint", ["time", "distance", "tot_dist", "speed", "coordinates"]
)


def read_gpx_array(file_name):
    """Read a gpx file and produce an array of points"""

    with open(file_name, "r") as gpx_file:
        gpx = gpx_parser.parse(gpx_file)

    speed_points = []

    for track in gpx:
        for segment in track:
            previous_point = None
            t = 0
            total_distance = 0
            for point in segment:
                if previous_point is not None:
                    distance = haversine(
                        (previous_point.latitude, previous_point.longitude),
                        (point.latitude, point.longitude),
                    )
                    delta_time = previous_point.time_difference(point)
                    if delta_time == 0:
                        continue
                    t += delta_time
                    total_distance += distance

                    speed = distance / delta_time * 3600 / 1000
                    speed_points.append(
                        GpxPoint(
                            t,
                            distance,
                            total_distance,
                            speed,
                            (point.latitude, point.longitude),
                        )
                    )
                previous_point = point

    return speed_points


SEG_LENGTH = 200


def cut_segment_array(pts, seg_length=SEG_LENGTH):
    """
    Compute the fluidity score for each segment of seg_length meters.
    """
    cur_seg = 0
    segment_pts = []
    segments = []
    for pt in pts:
        total_distance = pt[2]
        if not seg_length:
            continue
        seg = total_distance // seg_length
        if seg > cur_seg:
            seg_score = score(segment_pts)
            segments.append((cur_seg, seg_score, segment_pts))
            segment_pts = [pt]
            cur_seg = seg
        else:
            segment_pts.append(pt)

    return segments


def floatRgb(mag, cmin, cmax):
    """ Return a tuple of floats between 0 and 1 for R, G, and B.
    from https://www.oreilly.com/library/view/python-cookbook/0596001673/ch09s11.html
    """
    # Normalize to 0-1
    try:
        x = float(mag - cmin) / (cmax - cmin)
    except ZeroDivisionError:
        x = 0.5  # cmax == cmin
    blue = min((max((4 * (0.75 - x), 0.0)), 1.0))
    red = min((max((4 * (x - 0.25), 0.0)), 1.0))
    green = min((max((4 * math.fabs(x - 0.5) - 1.0, 0.0)), 1.0))
    return red, green, blue


def rgb(mag, cmin, cmax):
    """ Return a tuple of integers, as used in AWT/Java plots. """
    red, green, blue = floatRgb(mag, cmin, cmax)
    return int(red * 255), int(green * 255), int(blue * 255)


def strRgb(mag, cmin, cmax):
    """ Return a hex string, as used in Tk plots. """
    return "#%02x%02x%02x" % rgb(mag, cmin, cmax)


def to_geojson(segments):
    segs_array = []
    for segment in segments:
        seg_score = segment[1]
        seg_color = strRgb(seg_score, 0, 100)

        seg_dict = {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [long_, lat]
                    for t, distance, total_distance, speed, (lat, long_), in segment[2]
                ],
            },
            "properties": {
                "score": seg_score,
                "_umap_options": {"color": seg_color, "weight": 5 + seg_score/ 4, "opacity": 0.35+ seg_score/100},
            },
        }
        segs_array.append(seg_dict)

    trace = {"type": "FeatureCollection", "features": segs_array}
    return trace


MIN_SPEED = 5


def score(pts):
    """ Compute a fuidity """

    under = len([pt for pt in pts if pt[3] < MIN_SPEED])
    return under / len(pts) * 100


def score_by_time_under_threshold(pts):

    return len(pts[pts["speed"] < MIN_SPEED]) / len(pts) * 100


def analyse_single_file(path_file):

    pts = read_gpx_array(path_file)
    segments = cut_segment_array(pts)
    # print("Segments ", segments)
    geo_json = to_geojson(segments)
    return geo_json


def write_to_geojson(geo_json, out_file):
    import json

    # print(json.dumps(geo_json))
    with open(out_file, encoding="utf-8", mode="w") as f:
        f.write(json.dumps(geo_json))


def analyse_in_dir_single_out(path_dir, out_file):
    from pathlib import Path

    p = Path(path_dir)
    all_segments = []
    for gpx_file in p.glob("*.gpx"):
        print(f"Analysing single file '{gpx_file}'")

        try:
            pts = read_gpx_array(gpx_file)
            segments = cut_segment_array(pts)
            all_segments.extend(segments)
        except Exception as e:
            print(f"Error analysing '{gpx_file}'", e)
            break

    geo_json = to_geojson(all_segments)

    print(f"Writting geojson to '{out_file}''")
    write_to_geojson(geo_json, out_file)


def analyse_in_dir(path_dir):
    from pathlib import Path

    p = Path(path_dir)
    for gpx_file in p.glob("*.gpx"):
        print(f"Analysing single file '{gpx_file}'")

        geo_json = analyse_single_file(gpx_file)

        out_file = f"{gpx_file.stem}.json"

        print(f"Writting geojson to '{out_file}''")
        write_to_geojson(geo_json, out_file)


if __name__ == "__main__":
    import argparse, textwrap

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="A script to generate geojson file of segments with associated fluidity score",
        epilog=textwrap.dedent("""Examples :
        Analysing all files in './gpx_dir' and outputing a single 'segments.json' geojson file:
           python note_fluide.py -d --single_file  --out segments.json   ./gpx_dir

        Analysing a single gpx file, output is written to 'out.json
           python note_fluide.py  my_trace.gpx""")
    )
    parser.add_argument(
        "-d",
        "--directory",
        help="Analyse all files in the directory. When this flag is true, "
        "the last argument must be a directory that contains several *.gpx files",
        action="store_true",
        default=False,
    )

    parser.add_argument(
        "--single_file",
        action="store_true",
        help="Outputs all segments in a single geojson files. "
        "This flag is meant to be used when analysing several gpx file with the -d flag, "
        "a single geojson is generated that contains all segement from all gpx"
        "files. If this flag is not set, one geojson file is generated for each"
        "gpx file",
    )
    parser.add_argument(
        "-o",
        "--out",
        help="Name of the output geojson file. If not given, the output is written"
        "in 'out.json', except when analusing several gpx file without the "
        "'--single_file' flag. In that case each output file is named after the "
        "corresponding input gx file.",
        type=str,
        default="out.json",
    )
    parser.add_argument(
        "trace",
        help="Location of the gpx trace(s), this can be a single gpx file "
        "or, when using the '-d' flag a directory containing many gpx file",
    )

    args = parser.parse_args()
    print(args.directory)
    if args.directory:

        if args.single_file:
            print(f"Analysing files in {args.trace}, out in {args.out}")
            analyse_in_dir_single_out(args.trace, args.out)

        else:
            print(f"Analysing files in {args.trace}")
            analyse_in_dir(args.trace)
    else:
        print(f"Analysing single file '{args.trace}'")

        geo_json = analyse_single_file(args.trace)
        print(f"Writting geojson to '{args.out}''")
        write_to_geojson(geo_json, args.out)
